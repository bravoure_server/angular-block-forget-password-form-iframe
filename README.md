# Bravoure - Block Forget Password Iframe

## Directive: Code to be added:

        <block-forget-password-form-iframe></block-forget-password-form-iframe>

## Use

it takes the iframe from the core and it loads it

### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-block-forget-password-form-iframe": "1.0"
      }
    }

and the run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
