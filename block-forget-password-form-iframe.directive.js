(function () {
    'use strict';

    function blockForgetPasswordFormIframe (PATH_CONFIG, $rootScope, $sce, $state) {
        return {
            restrict: "E",
            replace: "true",
            link: function (scope, element, attrs) {

                // Content only Accesible for private page
                $rootScope.$emit('noPrivatePage');

            },
            controller: function ($scope) {

                $scope.forget_password_url = ($state.params.hash != undefined) ? host + 'password/reset?lang=' + PATH_CONFIG.current_language + '&password_reset_token=' + $state.params.hash : host + 'password/email?lang=' + PATH_CONFIG.current_language + '&redirect_uri=' + host + 'authorize' ;

                $scope.trustSrc = function (src) {
                    return $sce.trustAsResourceUrl(src);
                };

                $scope.showIframe = true;
                //($scope.$state.params.identifier == 'top.forget_password_page') ? true : false;

                if (!data.authenticationByPage) {
                    // functionalities to show/hide the iframe

                    $rootScope.$on('hideIframe', function () {
                        $scope.$apply(function () {
                            $scope.showIframe = false;
                        });
                    });

                    $rootScope.$on('showIframe', function () {
                        $scope.$apply(function () {
                            $scope.showIframe = true;
                        });
                    });
                }
            },
            templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-block-forget-password-form-iframe/block-forget-password-form-iframe.html'
        }
    }

    blockForgetPasswordFormIframe.$inject = ['PATH_CONFIG', '$rootScope', '$sce', '$state'];

    angular
        .module('bravoureAngularApp')
        .directive('blockForgetPasswordFormIframe', blockForgetPasswordFormIframe);
})();
